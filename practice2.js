// const array1 = [5, 12 , 8, 130, 44];

// const found = array1.find(element => element % 2 === 0);
// console.log(found);

// let fi = (element) => element % 2 === 0;
// console.log(array1.findIndex(fi));

// const inventory = [
// 	{name: 'apples', quantity: 2},
// 	{name: 'banana', quantity:0},
// 	{name: 'cherries', quantity:5}
// ];

// function isCherries(fruit){
// 	return fruit.name === 'apples';
// }

// console.log(inventory.find(isCherries));




// function isPrime(element, index, array1){
// 	let start = 2;
// 	while (start <= Math.sqrt(element)){
// 		if (element % start++ < 1){  		// 4 % 2 =0  then start++=> start=3
// 			return false;
// 		}
// 	}
// 	return element > 1;
// }
// console.log(array1.find(isPrime));


// const array = [0,1,,,,5,6];

// array.find(function (value, index) {
// 	console.log('visited index', index, 'with value', value);
// })

// array.find(function(value, index){
// 	if(index === 0) {
// 		console.log('deleting array[5] with value', array[5]);
// 		delete array[5];
// 	}
// 	console.log('visited index', index, 'with value', value);
// });

			//filter

// const word = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
// const result = word.filter(word => word.length > 6)
// console.log(result);


// function isBigEnough(value) {
// 	return value >= 10
// }

// let filtered = [12,5,8,130,44].filter(isBigEnough);
// console.log(filtered)



// const arr = [-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
// function isPrime(num){
// 	for (let i = 2; num > i; i++){
// 		if(num % i == 0){
// 			return false;
// 		}
// 	}
// 	return num > 1;
// }

// console.log(arr.filter(isPrime));

// let fruits = ['apple', 'banana','grapes','mango','orange'];

// function filteredItem(arr, query) {
// 	return arr.filter(el => el.toLowerCase().indexOf(query.toLowerCase()) !== -1)
// }
// console.log(filteredItem(fruits, 'ap'));
// console.log(filteredItem(fruits,'an'));


		//map

// const array1  = [1,4,9,16];
// //const mp1 = array1.map( x => x * 2);
// //const roots = array1.map((num) => Math.sqrt(num));
// let a = array1.map((el) =>{ console.log(el); return "souvik"});

// // console.log(mp1);
// // console.log(roots);
// console.log(a);
	

			//reduce

// const getMax = (a,b) => Math.max(a,b);

// let m = [1,100].reduce(getMax,50); //100
// console.log(m);

// let max = [  50].reduce(getMax, 10); //50
// console.log(max);

// let er = [ ].reduce(getMax,1); //when the value check max 1 then it givex 1
// console.log(er);

// let typeError = [ ].reduce(getMax); //it show type error
// console.log(typeError);

// function greeting(name) {
// 	console.log('Hello '+name);
// }

// function processUserInput(callback) {
// 	var name = console.log('please enter your name.');
// 	callback(name);
// }

// processUserInput(greeting);



// function addNumbers(firstNumber, secondNumber)
// {
// 	var returnValue = "result is : ";
// 	function add() 
// 	{
// 		return returnValue + (firstNumber + secondNumber)
// 	}
// 	return add();
// }

// var result = addNumbers(10,20);  // or var result = addNumbers(10,20)();
// console.log(result);


let arr = [
	{name: "s", date: "08/03/2022"},
	{name: "o", date: "09/03/2022"},
	{name: "u", date: "10/03/2022"},
	{name: "v", date: "11/03/2022"},
	{name: "i", date: "12/03/2022"}
];

let a = arr.map(ele => ele.date);
console.log(a);