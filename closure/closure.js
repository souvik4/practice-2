// function addNumbers(firstNumber, secondNumber)
// {
// 	var returnValue = "result is : ";
// 	function add() 
// 	{
// 		return returnValue + (firstNumber + secondNumber)
// 	}
// 	return add();
// }

// var result = addNumbers(10,20);  // or var result = addNumbers(10,20)();
// console.log(result);

class Person {
	constructor(name) {
		this.name = name;
	}

	printNameArrow() {
		setTimeout(() => {
			console.log('Arrow: ' + this.name)
		},100)
	}

	printNameFunction() {
		setTimeout(function(){
			console.log('Function: ' + this.name)
		},100)
	}
}


let person = new Person('Bob')
person.printNameArrow();
person.printNameFunction();
